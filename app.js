if (process.env.NODE_ENV !== "production") {
	require('dotenv').config();
}

// require('dotenv').config();



const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const session = require('express-session');
const flash = require('connect-flash');
const ejsMate = require('ejs-mate');
const catchAsync = require('./utils/catchAsync');
const ExpressError = require('./utils/ExpressError');
const { campgroundSchema, reviewSchema } = require('./schemas.js');
const methodOverride = require('method-override');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const User = require('./models/user');
const helmet = require('helmet');
const MongoStore = require('connect-mongo');
const dbUrl = process.env.DB_URL || 'mongodb://localhost:27017/camp-ground';
const secret = process.env.SECRET || "secretwalangclue";
//REDEPLOYMENT
const store = MongoStore.create({
    mongoUrl: dbUrl,
    touchAfter: 24 * 60 * 60,
    crypto: {
        secret
    }
});



store.on('error',function(e) {
	console.log('Session Store Error', e)
})

const mongoSanitize = require('express-mongo-sanitize');


const userRoutes = require('./routes/users')
const campgroundsRoutes = require('./routes/campgrounds');
const reviewsRoutes = require('./routes/reviews');





mongoose.connect(dbUrl, {
	useNewUrlParser :true,
	useCreateIndex :true,
	useUnifiedTopology :true,
	useFindAndModify :false
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
	console.log('Database Connected');
});
const app = express();


//enable us to use ejs-mate
app.engine('ejs',ejsMate)

//allowing ejs to work with
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'));


//we need this in order to tell express to parse post request.body
app.use(express.urlencoded({extended: true}));
//we need to in order for method-override to work
app.use(methodOverride('_method'));

app.use(express.static(path.join(__dirname, 'public')));

app.use(mongoSanitize());

const sessionConfig = {
//httponly adds extra layer of protection for cookies
	store,
	name: 'session',
	secret,
	resave: false,
	saveUninitialized: true,
	cookie: {
		httpOnly: true,
		// secure: true,
		expires: Date.now() + 1000 * 60 * 60 * 24 * 7,
		maxAge: 1000 * 60 * 60 * 24 * 7
	}
}

app.use(session(sessionConfig));
app.use(flash());
app.use(helmet());

const scriptSrcUrls = [
    "https://stackpath.bootstrapcdn.com/",
    "https://api.tiles.mapbox.com/",
    "https://api.mapbox.com/",
    "https://kit.fontawesome.com/",
    "https://cdnjs.cloudflare.com/",
    "https://cdn.jsdelivr.net",
];
const styleSrcUrls = [
    "https://kit-free.fontawesome.com/",
    "https://stackpath.bootstrapcdn.com/",
    "https://api.mapbox.com/",
    "https://api.tiles.mapbox.com/",
    "https://fonts.googleapis.com/",
    "https://use.fontawesome.com/",
];
const connectSrcUrls = [
    "https://api.mapbox.com/",
    "https://a.tiles.mapbox.com/",
    "https://b.tiles.mapbox.com/",
    "https://events.mapbox.com/",
];
const fontSrcUrls = [];
app.use(
    helmet.contentSecurityPolicy({
        directives: {
            defaultSrc: [],
            connectSrc: ["'self'", ...connectSrcUrls],
            scriptSrc: ["'unsafe-inline'", "'self'", ...scriptSrcUrls],
            styleSrc: ["'self'", "'unsafe-inline'", ...styleSrcUrls],
            workerSrc: ["'self'", "blob:"],
            childSrc: ["blob:"],
            objectSrc: [],
            imgSrc: [
                "'self'",
                "blob:",
                "data:",
                "https://res.cloudinary.com/dzc3ezwun/",  
                "https://images.unsplash.com/",
            ],
            fontSrc: ["'self'", ...fontSrcUrls],
        },
    })
);

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//flash middleware
app.use((req, res, next) => {
	console.log(req.query)
	res.locals.currentUser = req.user;
	res.locals.success = req.flash('success');
	res.locals.error = req.flash('error');
	next();
})

// app.get('/fakeUser', async (req, res) => {
//    const user = new User({email:"sample@gmail.com", username: "sample"})
//    const newUser = await User.register(user, 'mypassword')
//    res.send(newUser);
// })

//register - fORM
//POST register - create a user



app.use("/", userRoutes)
app.use("/campgrounds", campgroundsRoutes)
app.use("/campgrounds/:id/reviews", reviewsRoutes)

app.get('/', (req,res) => {
	res.render('home')
})





//404 error response from ExpressError
app.all('*', (req, res, next) => {
	next(new ExpressError('PAGE NOT FOUND :( ',404))
})


//async error response
app.use((err, req, res, next) => {
	const {statusCode = 500 } = err;
	 if (!err.message) err.message = "Oof! Something Went Wrong!"
	  res.status(statusCode).render('error',{err})
})


// app.get('/newcampground', async (req,res) => {
// 	//TRIAL OF CREATING OUR NEW CAMPGROUND
// 	const camp = new Campground({title: 'Antipolo', description: 'best place to chill'})
// 	await camp.save();
// 	res.send(camp)
// })

const port = process.env.PORT || 8075;
app.listen(port, function () {
  console.log(`Listening on port ${port}`);
});