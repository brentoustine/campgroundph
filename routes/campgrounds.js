const express = require('express');
const router = express.Router();
const catchAsync = require('../utils/catchAsync');
const campgrounds = require('../controllers/campgrounds');
const ExpressError = require('../utils/ExpressError');
const Campground = require('../models/campground');
const {isLoggedIn, isAuthor, validateCampground} = require('../middleware');
const multer  = require('multer');
const {storage} = require('../cloudinary');
const upload = multer({ storage});




router.route('/')
      .get(catchAsync(campgrounds.index))

      //we need app.use(express.urlEnconded({extended: true})) for this to parse the req.body
      .post(isLoggedIn, upload.array('image'), validateCampground, catchAsync(campgrounds.createCampground))

      //cloudinary test
      // .post(upload.array('image'), (req,res) => {
      // 	console.log(req.body, req.files);
      // 	res.send("Testing Success!")
      // })

//just to render our new campground form no need for async
router.get('/new', isLoggedIn, campgrounds.renderNewForm);

router.route('/:id')
      //Show Page
      .get(catchAsync(campgrounds.showCampgrounds))

      //Updating Campground 
      .put(isLoggedIn, isAuthor, upload.array('image'), validateCampground, catchAsync(campgrounds.updateCampground))

      //Deleting Campground
      .delete(isLoggedIn, isAuthor, catchAsync(campgrounds.deleteCampground))

//our route to serve our edit form
router.get('/:id/edit', isLoggedIn, isAuthor, catchAsync(campgrounds.renderEditForm))






module.exports = router;