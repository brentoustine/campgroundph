const mongoose = require('mongoose');
const cities = require('./cities')
const Campground = require('../models/campground')
const {places, descriptors} = require('./seedHelpers')

mongoose.connect('mongodb://localhost:27017/camp-ground', {
	useNewUrlParser : true,
	useCreateIndex :true,
	useUnifiedTopology : true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
	console.log('Database Connected');
});

const sample = array => array[Math.floor(Math.random() * array.length)]



const seedDB = async () => {
	await Campground.deleteMany({});
	for (let i = 0; i < 50; i++) {
     const random1000 = Math.floor(Math.random() * 1000);
     const price = Math.floor(Math.random() * 20) + 10;
     const camp = new Campground({
     	//MY USER ID
     	author:'614447ec73a6e001441f784b',
     	location: `${cities[random1000].city},${cities[random1000].state}`,
     	title: `${sample(descriptors)} ${sample(places)}`,
     	description: 'Lorem ipsum, dolor sit, amet consectetur adipisicing elit. Quisquam amet assumenda id impedit perferendis iusto illo, provident doloribus et voluptates quibusdam delectus quia dicta ullam dolor dolorum accusantium blanditiis commodi.',
     	price,
     	geometry: { 
        type: "Point",
     		coordinates:[
        cities[random1000].longitude,
        cities[random1000].latitude,

        ]
     	},
     	images :  [
    {
      url: 'https://res.cloudinary.com/dzc3ezwun/image/upload/v1632992785/CampgroundPH/gnj6wsmwbajt03akakm1.jpg',
      filename: 'CampgroundPH/gnj6wsmwbajt03akakm1'
    },
    {
      url: 'https://res.cloudinary.com/dzc3ezwun/image/upload/v1632992787/CampgroundPH/qmuuxeh3we8wdoglyzht.jpg',
      filename: 'CampgroundPH/qmuuxeh3we8wdoglyzht'
    },
    {
      url: 'https://res.cloudinary.com/dzc3ezwun/image/upload/v1632992790/CampgroundPH/lcbuobjdtkfaw8c7n9ws.jpg',
      filename: 'CampgroundPH/lcbuobjdtkfaw8c7n9ws'
    }
  ]

     })
       await camp.save();
	}
}

seedDB().then(() => {
	mongoose.connection.close()
});